import EventEmitter from "eventemitter3";

export default class Application extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }

  constructor() {
    super();
    this._loading = document.querySelector('progress');
    this._startLoading();
    this.array = [];
    this.emit(Application.events.READY);
  }

  _render({ displayName, restart, hostName }) {
    return `
      <article class="media">
        <div class="media-content">
          <div class="content">
          <h4>displayName: ${displayName}</h4>
            <p>
              <span class="tag">restartRequired: ${restart}</span> <span class="tag">${hostName}</span>
              <br>
            </p>
          </div>
        </div>
      </article>
    `;
  }

  async _load() {
    let response = await fetch(
      '', {
      headers: {
        Accept: 'application/json',
        Authorization: ''
      },
    });
    let responseJSON = await response.json();

    let displayName = null;
    let restart = false;
    let hostName = null;

    for(let i = 0; i < responseJSON.length; i++){
      if(responseJSON[i].monitoringState.restartRequired == true){
        displayName = responseJSON[i].displayName;
        restart = responseJSON[i].monitoringState.restartRequired;
        hostName = responseJSON[i].fromRelationships.isProcessOf;
        this._create({ displayName, restart, hostName})
      }
    }
    this._stopLoading();
  }

  _create({ displayName, restart, hostName }) {
    const box = document.createElement("div");
    box.classList.add("box");
    box.innerHTML = this._render({
      displayName: displayName,
      restart: restart,
      hostName: hostName,
    });
    document.body.querySelector(".main").appendChild(box);
  }

  async _startLoading() {
    await this._load();
  }

  _stopLoading() {
    this._loading.style.display = 'none';
  }
}
